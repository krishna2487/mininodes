// **********************************************************************************
// Struct Send RFM69 Example
// **********************************************************************************
// Copyright Felix Rusu 2018, http://www.LowPowerLab.com/contact
// **********************************************************************************
// License
// **********************************************************************************
// This program is free software; you can redistribute it 
// and/or modify it under the terms of the GNU General    
// Public License as published by the Free Software       
// Foundation; either version 3 of the License, or        
// (at your option) any later version.                    
//                                                        
// This program is distributed in the hope that it will   
// be useful, but WITHOUT ANY WARRANTY; without even the  
// implied warranty of MERCHANTABILITY or FITNESS FOR A   
// PARTICULAR PURPOSE. See the GNU General Public        
// License for more details.                              
//                                                        
// Licence can be viewed at                               
// http://www.gnu.org/licenses/gpl-3.0.txt
//
// Please maintain this license information along with authorship
// and copyright notices in any redistribution of this code
// **********************************************************************************
#include <RFM69.h>         //get it here: https://www.github.com/lowpowerlab/rfm69
#include <RFM69_ATC.h>     //get it here: https://www.github.com/lowpowerlab/rfm69
#include <SPIFlash.h>      //get it here: https://www.github.com/lowpowerlab/spiflash
#include <SPI.h>           //included with Arduino IDE install (www.arduino.cc)
#include <SparkFun_Si7021_Breakout_Library.h>
#include <BH1750.h>
#include <LowPower.h>
//*********************************************************************************************
//************ IMPORTANT SETTINGS - YOU MUST CHANGE/CONFIGURE TO FIT YOUR HARDWARE *************
//*********************************************************************************************
#define NODEID      90
#define NETWORKID   101
#define GATEWAYID   1
//Match frequency to the hardware version of the radio on your Moteino (uncomment one):
#define FREQUENCY     RF69_433MHZ
//#define FREQUENCY     RF69_868MHZ
// #define FREQUENCY     RF69_915MHZ
#define ENCRYPTKEY    "sampleEncryptKey" //has to be same 16 characters/bytes on all nodes, not more not less!
// #define IS_RFM69HW_HCW  //uncomment only for RFM69HW/HCW! Leave out if you have RFM69W/CW!
//*********************************************************************************************
//Auto Transmission Control - dials down transmit power to save battery
//Usually you do not need to always transmit at max output power
//By reducing TX power even a little you save a significant amount of battery power
//This setting enables this gateway to work with remote nodes that have ATC enabled to
//dial their power down to only the required level
// #define ENABLE_ATC    //comment out this line to disable AUTO TRANSMISSION CONTROL
//*********************************************************************************************
#define SERIAL_BAUD 115200
#define MY_CORE_ONLY

#if defined (MOTEINO_M0) && defined(SERIAL_PORT_USBVIRTUAL)
  #define Serial SERIAL_PORT_USBVIRTUAL // Required for Serial on Zero based boards
#endif

#ifdef ENABLE_ATC
  RFM69_ATC radio;
#else
  RFM69 radio;
#endif

// const int INT_PIN = 2;

SPIFlash flash(8); //EF40 for 16mbit windbond chip
// SPIFlash flash(8, 0xEF30); //EF40 for 16mbit windbond chip

uint32_t TRANSMITPERIOD = 10000; //transmit a packet to gateway so often (in ms)
byte sendSize=0;
boolean requestACK = false;


struct __attribute__((__packed__)) Payload {                // Radio packet structure max 61 bytes or 57 id SessionKey is used
  long int      nodeId; //store this nodeId
  long int      frameCnt;// frame counter
  unsigned long uptime; //uptime in ms
  float         temp;   //temperature maybe??
  float         humidity;
  float         lux;
  float         battery;
};
Payload theData;

Weather sensor;
BH1750 lightMeter(0x23);

// void wakeUp()
// {


// }
long lastPeriod = -1;
float lux=0;
uint8_t count=0;
void setup() {

  Serial.begin(SERIAL_BAUD);
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  radio.encrypt(ENCRYPTKEY);
  char buff[50];
  sprintf(buff, "\nTransmitting at %d Mhz...", FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915);
  Serial.println(buff);
  
    Serial.println ("\n****************************************************************\n");
  Serial.print("Node ID: ");
  Serial.println(NODEID);
  Serial.print("Network ID: ");
  Serial.println(NETWORKID);
  Serial.print("Gateway ID: ");
  Serial.println(GATEWAYID);
  Serial.println ("\n****************************************************************\n");

  if (flash.initialize())
    Serial.println("SPI Flash Init OK!");
  else
    Serial.println("SPI Flash Init FAIL! (is chip present?)");
    // sensor.begin();
    // if (lightMeter.begin(BH1750::CONTINUOUS_HIGH_RES_MODE)) {
    if (lightMeter.begin(BH1750::ONE_TIME_LOW_RES_MODE)) {
    Serial.println(F("BH1750 Advanced begin"));
  }
  else {
    Serial.println(F("Error initialising BH1750"));
  }
    // radio.sleep();
    delay(100);
    Serial.println("Placing radio to sleep");
    // LowPower.idle(SLEEP_8S, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_OFF, 
    //             SPI_OFF, USART0_OFF, TWI_OFF);
    flash.sleep();
    // radio.setPowerLevel(0);
    radio.receiveDone();
    delay(100);
    radio.sleep();
    Serial.println("Placing node to sleep");
    delay(100);
    count=79;
    // LowPower.idle(SLEEP_8S,ADC_OFF,TIMER2_OFF,TIMER1_OFF,TIMER0_OFF,SPI_OFF,USART0_OFF,TWI_OFF);
    delay(100);
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF); 

}

void loop() {

    if(count == 80)
    {

        Serial.println("Sending data to Gateway");
        theData.nodeId = NODEID;
        theData.uptime = millis();
        theData.temp = (float)sensor.getTemp(); //it's hot!
        theData.humidity = (float)sensor.getRH();
        theData.lux = (float) lightMeter.readLightLevel();
        theData.battery = analogRead(A6);
        Serial.print("Sending struct (");
        Serial.print(sizeof(theData));
        Serial.print(" bytes) ... ");
        if (radio.sendWithRetry(GATEWAYID, (const void*)(&theData), sizeof(theData)))
          Serial.print(" ok!");
        else Serial.print(" nothing...");
        Serial.println();
        count=0;

       radio.sleep();
    }

  count++;
  Serial.println ("Powering down..");
  delay (100);
  // LowPower.idle(SLEEP_8S,ADC_OFF,TIMER2_OFF,TIMER1_OFF,TIMER0_OFF,SPI_OFF,USART0_OFF,TWI_OFF);
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF); 

}

